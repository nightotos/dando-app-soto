import axios from 'axios';

const paisesApi = axios.create({
  baseURL: 'https://restcountries.com/v2/regionalbloc/usan',
});

export default paisesApi;
